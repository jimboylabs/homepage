---
name: musictracker
url: https://musictracker.fly.dev
repo: https://gitlab.com/jimboylabs/musictracker
description: personal music scrobbler, using laravel and sqlite
---
