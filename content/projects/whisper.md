---
name: whisper.go
url: https://gitlab.com/jimboylabs/whisper.go
repo: https://gitlab.com/jimboylabs/whisper.go
description: transcribe mp3 to text, using whisper.cpp and temporal.io
---
