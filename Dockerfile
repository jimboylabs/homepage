FROM docker.io/oven/bun:1 AS public

COPY package.json bun.lockb /app/

WORKDIR /app

RUN bun install

COPY . .

RUN bun run build

FROM docker.io/serversideup/php:8.3-cli as vendor

COPY --chown=www-data:www-data . /var/www/html

RUN composer install --no-interaction --optimize-autoloader --ignore-platform-reqs --no-dev

FROM docker.io/dunglas/frankenphp:static-builder-1.2.5 AS builder

ENV NO_COMPRESS=1

# Copy your app
WORKDIR /go/src/app/dist/app

# Remove the tests and other unneeded files to save space
# Alternatively, add these files to a .dockerignore file
RUN rm -Rf tests/

COPY --from=vendor /var/www/html /go/src/app/dist/app
COPY --from=public /app/public/build /go/src/app/dist/app/public/build

#RUN composer dump-autoload -o

# Build the static binary, be sure to select only the PHP extensions you want
WORKDIR /go/src/app/

RUN EMBED=dist/app/ ./build-static.sh

FROM docker.io/alpine:3.19.1

ARG build=dev

ENV BUILD $build

WORKDIR /app

COPY --from=builder /go/src/app/dist/frankenphp-linux-x86_64 home

ENTRYPOINT ["/app/home"]