<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Orbit\Concerns\Orbital;

class Project extends Model
{
    use HasFactory, Orbital;

    protected $guarded = [];

    public static function schema(Blueprint $table)
    {
        $table->string('name');
        $table->string('url');
        $table->string('repo');
        $table->string('description');
    }
}
