<?php
$projects = App\Models\Project::all();
?>

@extends('layouts.app')

@section('content')
<div class="mb-12">
    <h2 class="mb-4">@wayanjimmy</h2>
    <p>I'm Web Developer live in <x-highlight>Jakarta, Indonesia</x-highlight> who is interested in <x-highlight>full<s>snack</s>stack development</x-highlight> and shipping <x-highlight>apps.</x-highlight></p>
    <p>I'm  an anime lover,  digital hoarder and love to talk about personal development and computers.</p>
</div>
<div class="mb-12">
    <h2 class="mb-4">projects</h2>
    <ul class="list-disc ml-5">
        @foreach($projects as $project)
        <li>
            <a class="text-black no-underline cursor-pointer hover:font-bold hover:border-black" target="_blank" href="{{ $project->url }}">{{ $project->name }}</a> - {{ $project->description }}
            <a class="text-black no-underline cursor-pointer hover:font-bold hover:border-black" target="_blank" href="{{ $project->repo}}">[code]</a>
        </li>
        @endforeach
    </ul>
</div>
<div class="mb-12">
    <ul>
        <li>
            <a class="text-black no-underline hover:underline" href="https://t.me/wayanjimmy" rel="me" target="_blank">telegram</a>
        </li>
        <li>
            <a class="text-black no-underline hover:underline" href="https://memos.wayanjimmy.xyz/u/wayanjimmy" rel="me" target="_blank">memos</a>
        </li>
    </ul>
</div>
@endsection
