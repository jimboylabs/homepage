<span class="text-blue-700 font-semibold no-underline cursor-pointer">
    {{ $slot }}
</span>
