<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite(['resources/css/app.css'])
    @if(app()->environment('production'))
    <script defer data-domain="wayanjimmy.xyz" src="https://analytics.rollingsayu.xyz/js/script.outbound-links.js"></script>
    <script>window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }</script>
    @endif
    <title>Wayan Jimmy - Web Developer</title>
</head>
<body class="font-mono">
    <div class="container px-8 py-8 mx-auto lg:px-64 md:px-32">
        @yield('content')
    </div>
</body>
</html>
